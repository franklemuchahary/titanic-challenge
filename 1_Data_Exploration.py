
# coding: utf-8

# In[1]:

import pandas as pd
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from collections import Counter
import seaborn as sns
from ggplot import *
import re


# ### Import the data

# In[2]:

train = pd.read_csv('train.csv', sep=',')
test = pd.read_csv('test.csv')
sample_submission = pd.read_csv('gender_submission.csv')


# In[3]:

train_new = train.drop('Survived', axis=1)
full = train_new.append(test, ignore_index=True)


# ### Exploratory Data Analysis

# In[4]:

full


# In[5]:

labels = np.repeat(['All'],len(full['Fare']))
bp_data1 = pd.DataFrame({'Fare':list(full['Fare']), 'labels':labels})
ggplot(aes(y='Fare', x='labels'), data=bp_data1) + geom_boxplot() + ylim(0,250) + theme_bw()


# In[6]:

full.boxplot(column='Age')
plt.show()
#train['Age'].value_counts()


# In[7]:

full['Cabin'].isnull().sum()


# In[8]:

full['Embarked'].isnull().sum()


# In[9]:

full['Fare'].isnull().sum()


# In[10]:

full['Pclass'].value_counts()


# In[100]:

full['Age'].isnull().sum()


# ### Feature Engineering

# In[101]:

#creating a variable called family size
full['fam_size'] = full['SibSp'] + full['Parch'] + 1
full


# In[102]:

#extracting titles out of names
def match_string(my_str):
    m = re.search(r"(\s)+(\w+)(\.)", my_str)
    return m.group(2)
    
full['Title'] = full['Name'].apply(match_string)


# In[14]:

full


# In[103]:

#getting all unique titles
titles_dic = Counter(list(full['Title']))
titles_list = []

for i in titles_dic:
    titles_list.append(i)
    
print titles_list


# In[104]:

#replacing titles and converting into 4 categories
def replace_titles(x):
    title=x['Title']
    if title in ['Don', 'Major', 'Capt', 'Jonkheer', 'Rev', 'Col', 'Sir']:
        return 'Mr'
    elif title in ['Countess', 'Mme', 'Lady']:
        return 'Mrs'
    elif title in ['Mlle', 'Ms', 'Dona']:
        return 'Miss'
    elif title =='Dr':
        if x['Sex']=='Male':
            return 'Mr'
        else:
            return 'Mrs'
    else:
        return title
    
full['Title']=full.apply(replace_titles, axis=1)


# In[105]:

new_titles_dict = Counter(full['Title'])
print new_titles_dict

#fill missing age values for people with  different titles
for i in new_titles_dict:
    full.loc[full['Title']==i,'Age'] = full.loc[full['Title']==i,'Age'].fillna(np.mean(full.loc[full['Title']==i,'Age']))
# In[107]:

full.loc[full['Age'].isnull(), 'Age'] = '###' 


# In[19]:

#plot to analyze missing embarked values and fare values
ggplot(aes(x='Pclass',y='Fare'), data=full) +    geom_boxplot() +    facet_wrap('Embarked', scales='free') +    ylim(0,200) + theme_bw()


# In[20]:

full[full['Fare'].isnull()]


# In[21]:

#fill missing fare values
mean_fare_temp = full[(full['Pclass']==3) & (full['Embarked']=='S') & (full['fam_size']==1)]['Fare'].mean()
full.loc[full['Fare'].isnull(),'Fare'] = mean_fare_temp


# In[22]:

full[full['Embarked'].isnull()]


# In[23]:

#from the plots missing values of Embarked potentially looks like C so fill it with them
full.loc[full['Embarked'].isnull(), 'Embarked'] = 'C'


# In[24]:

#get the suffix of the ticket
Counter(full['Ticket'])


# In[25]:

regex = r"^([a-zA-Z])+(\w*\.*\w*\.*\/*\w+\.*\w*\.*)+"

def get_ticket_prefix(tkt):
    m = re.search(regex, tkt)
    if m is not None:
        return m.group(0)
    else:
        return 'NO_PREF'

full['ticket_prefix'] = full['Ticket'].apply(get_ticket_prefix)


# In[26]:

full['ticket_prefix'].value_counts()


# ###### Dealing with missing Cabin and filling cabin

# In[30]:

full['Cabin'].value_counts()


# In[85]:

full.loc[full['Cabin'].isnull(), 'Cabin'] = '###UNKNOWN'

regex = r"^([a-zA-Z]{1})"
def get_cabin_prefix(cabin):
    m = re.search(regex, cabin)
    if m is not None:
        return m.group(1)
    else:
        return 'UNKNOWN'
    
full['Cabin_prefix'] = full['Cabin'].apply(get_cabin_prefix)


# In[45]:

ggplot(aes(x='Pclass', y='Fare', color='Embarked'), data=full) + geom_boxplot(alpha=0.5) +    facet_wrap('Cabin_prefix', scales='free') +    ylim(0,200)


# In[90]:

full['Cabin_prefix'].value_counts()


# In[87]:

full['Cabin_prefix'].isnull().sum()


# In[89]:

def fill_unknown_cabin_prefix(zzz):
    cbn_pfx = zzz['Cabin_prefix']
    pcls = zzz['Pclass']
    fr = zzz['Fare']
    fam_sz = zzz['fam_size']
    embk = zzz['Embarked']
    if cbn_pfx == 'UNKNOWN':
        if pcls == 1:
            if fr > 25 and fr < 70:
                return 'A'
            elif fr > 150:
                return 'B'
            elif fr > 70 and fr < 150:
                return 'C'
            else:
                return 'E'
        elif pcls == 2:
            if fr >10 and fr < 45:
                return 'F'
            else:
                return 'E'
        else:
            if fr > 10 and fr < 20:
                return 'G'
            else:
                return 'UNKNOWN'
    else:
        return cbn_pfx
            
full['Cabin_prefix'] = full.apply(fill_unknown_cabin_prefix, axis=1)                


# ### Create new csv of cleaned data for now

# In[91]:

test_cleaned = full.iloc[891:]
train_cleaned = full.iloc[:891]


# In[92]:

test_cleaned = test_cleaned.reset_index(drop=True)


# In[93]:

test_cleaned.to_csv("test_cleaned.csv",sep=",",index=False)


# In[94]:

train_labels = train[['PassengerId', 'Survived']]


# In[95]:

train_cleaned = train_cleaned.merge(train_labels, on="PassengerId")


# In[96]:

train_cleaned.to_csv("train_cleaned.csv", sep=",", index=False)

