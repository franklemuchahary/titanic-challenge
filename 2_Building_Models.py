
# coding: utf-8

# In[2]:

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

pd.set_option('display.max_columns', None)


# In[9]:

X = pd.read_csv('train_cleaned.csv', usecols=range(0,15))
y = pd.read_csv('train_cleaned.csv', usecols=['PassengerId','Survived'])


# In[10]:

X.shape


# In[11]:

X_test = pd.read_csv('test_cleaned.csv', usecols=range(0,15))
X_test.shape


# In[12]:

# Contactenate X and test to have the same things
frames = [X, X_test]
X_join = pd.concat(frames, keys=['train', 'test'])


# In[14]:

X_join.head()


# ### One Hot Encoding and Feature Scaling

# In[15]:

#sklearn imports
from sklearn import preprocessing
from sklearn import cross_validation


# In[16]:

#function to do one hot encoding
def do_one_hot_encoding(df_name, df_column_name, pref='', suffix=''):
    temp = pd.get_dummies(df_name[df_column_name], prefix=pref)
    df_name = df_name.join(temp, lsuffix=suffix)
    df_name = df_name.drop(df_column_name, axis=1) 
    return df_name


# In[17]:

#function to do label encoding
def label_encoding_func(df_name, df_col_name):
    le = preprocessing.LabelEncoder()
    le.fit(df_name[df_col_name])
    return le.transform(df_name[df_col_name])


# In[18]:

#remove unwanted columns
X_join = X_join.drop(['Name','Ticket','Cabin'], axis=1)


# In[19]:

#one hot encode Cabin
X_join = do_one_hot_encoding(X_join, 'Cabin_prefix', 'Cbn_')


# In[20]:

#one hot encode sex
X_join['Sex'] = label_encoding_func(X_join, 'Sex')


# In[21]:

#one hot encode embarked
X_join = do_one_hot_encoding(X_join, 'Embarked', 'EMBK_')


# In[22]:

#one hot encode title
X_join = do_one_hot_encoding(X_join, 'Title', 'Title_')


# In[23]:

#one hot encode Pclass
X_join = do_one_hot_encoding(X_join, 'Pclass', 'Pcls_')


# In[24]:

#label_encode Ticket Prefix
X_join['ticket_prefix'] = label_encoding_func(X_join, 'ticket_prefix')


# In[25]:

#one hot encode family size
X_join = do_one_hot_encoding(X_join, 'fam_size', 'fam_sz')


# ##### Separate back the dataset into train and test

# In[26]:

X_train = X_join.loc['train']
X_test = X_join.loc['test']


# In[28]:

print(X_train.shape)
print(X_test.shape)


# ##### Scaling

# In[29]:

feature_names_train = list(X_train.columns.values)
feature_names_test = list(X_test.columns.values)


# In[30]:

std_Scaler = preprocessing.StandardScaler()
min_max_Scaler = preprocessing.MinMaxScaler()
X_train_scaled = min_max_Scaler.fit_transform(X_train)
X_test_scaled = min_max_Scaler.fit_transform(X_test)


# In[31]:

X_train_scaled = pd.DataFrame(X_train_scaled)
X_test_scaled = pd.DataFrame(X_test_scaled)


# In[32]:

X_train_scaled.head()


# In[33]:

X_test_scaled.head()


# In[34]:

#split into training and test data
train_x, test_x, train_y, test_y = cross_validation.train_test_split(X_train_scaled, y['Survived'], train_size=0.8,
                                                                    random_state=777)


# In[36]:

#see if class is balanced, if not calculate class weights
class_labels_dict = dict(train_y.value_counts())
print(class_labels_dict)

from sklearn.utils import class_weight
class_weight = class_weight.compute_class_weight('balanced', np.unique(train_y), train_y)
class_weight_dict = {0:class_weight[0], 1:class_weight[1]}
class_weight_dict


# In[37]:

from sklearn import metrics

#function to calculate accurary
def give_accuracy(true, preds):
    return metrics.accuracy_score(true, preds)


# ### Train Models

# In[38]:

#import models from sklearn
from sklearn.grid_search import GridSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import BaggingClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from xgboost import XGBClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.neural_network import MLPClassifier

xgb2 = XGBClassifier(
    learning_rate =0.1,
    n_estimators=1300,
    min_child_weight=10,
    max_depth =7,
    gamma=10,
    subsample=0.9,
    colsample_bytree=0.9,
    nthread=4,
    scale_pos_weight=1.2,
    seed=77
)

xgb2.fit(X_train_scaled.iloc[:,fts_list], y['Survived'])
print np.mean(cross_validation.cross_val_score(xgb2, X_train_scaled.iloc[:,fts_list], y['Survived'], cv=7, 
                                               scoring='recall'))

xgb2_preds_test = xgb2.predict(X_test_scaled.iloc[:,fts_list])
# In[172]:

#random forest classifier
params = {'bootstrap':[True], 'n_estimators':[100]}

rfm = GridSearchCV(RandomForestClassifier(max_depth=7, class_weight=class_weight_dict), params, n_jobs=-1, cv=7, 
                  scoring='recall')
#rfm = RandomForestClassifier(class_weight=class_weight_dict, n_estimators=1200, bootstrap=True)
rfm = rfm.fit(train_x, train_y)
rfm_preds = rfm.predict(test_x)

rfm_preds_proba = rfm.predict_proba(train_x)
rfm_preds_proba_test = rfm.predict_proba(test_x)

rfm_preds_test = rfm.predict_proba(X_test_scaled)
rfm_preds_train = rfm.predict_proba(X_train_scaled)
#print np.mean(cross_validation.cross_val_score(rfm, X_train_scaled, y['Survived'], cv=6, scoring='accuracy'))

print (give_accuracy(test_y, rfm_preds))
print (params)
print (rfm.best_params_, rfm.best_score_) 

features = pd.DataFrame()
features['feature'] = train_x.columns.values
features['importance'] = rfm.feature_importances_
features.sort_values(by=['importance'], ascending=False, inplace=True)
features.set_index('feature', inplace=True)


features.plot(kind='barh', figsize=(20, 20))
plt.show()
# In[173]:

#bagging classifier
params = {'bootstrap':[True], 'n_estimators':[10]}

bc = GridSearchCV(BaggingClassifier(), params, n_jobs=-1, cv=6)
bc = bc.fit(train_x, train_y)
bc_preds = bc.predict(test_x)

bc_preds_proba = bc.predict_proba(train_x)
bc_preds_proba_test = bc.predict_proba(test_x)


bc_preds_test = bc.predict_proba(X_test_scaled)
bc_preds_train = bc.predict_proba(X_train_scaled)

print (give_accuracy(test_y, bc_preds))
print (params)
print (bc.best_params_, bc.best_score_)


# In[176]:

#logistic regression
params = {'penalty':['l2','l1'], 'C':np.linspace(1, 50, 10)}

lr = GridSearchCV(LogisticRegression(), params, n_jobs=-1, cv=6, scoring='recall')
lr = lr.fit(train_x, train_y)
lr_preds = lr.predict(test_x)

lr_preds_proba = lr.predict_proba(train_x)
lr_preds_proba_test = lr.predict_proba(test_x)

lr_preds_test = lr.predict_proba(X_test_scaled)
lr_preds_train = lr.predict_proba(X_train_scaled)

print (give_accuracy(test_y, lr_preds))
print (params)
print (lr.best_params_, lr.best_score_)


# In[177]:

#svm
params = {'C': np.logspace(0.01, 3, 10) ,'gamma': [0.01], 'kernel': ['rbf'], 'probability':[True]}

svc = GridSearchCV(SVC(class_weight=class_weight_dict), params, n_jobs = -1, cv=6, scoring='recall')
svc = svc.fit(train_x, train_y)
svc_preds = svc.predict(test_x)

svc_preds_proba = svc.predict_proba(train_x)
svc_preds_proba_test = svc.predict_proba(test_x)


svc_preds_test = svc.predict_proba(X_test_scaled)
svc_preds_train = svc.predict_proba(X_train_scaled)

print(give_accuracy(test_y, svc_preds))
print(params)
print("Best C & associated score", svc.best_params_, svc.best_score_)


# In[180]:

#xgboost
params =  {'learning_rate':[0.09], 'n_estimators':np.logspace(0.01,3,10, dtype=int), 'scale_pos_weight':[1.2]}

xgb = GridSearchCV(XGBClassifier(max_depth=6), params, n_jobs=-1, cv=6)
xgb = xgb.fit(train_x, train_y)
xgb_preds = xgb.predict(test_x)

xgb_preds_proba = xgb.predict_proba(train_x)
xgb_preds_proba_test = xgb.predict_proba(test_x)

xgb_preds_test = xgb.predict_proba(X_test_scaled)
xgb_preds_train = xgb.predict_proba(X_train_scaled)



print (give_accuracy(test_y, xgb_preds))
print (params)
print("Best C & associated score", xgb.best_params_, xgb.best_score_)


# In[246]:

#extratrees classifier
params = {'criterion': ['entropy'],'n_estimators':[50]}
etc = GridSearchCV(ExtraTreesClassifier(class_weight=class_weight_dict), params, n_jobs = -1, cv=7,
                   scoring='recall')
etc = etc.fit(train_x, train_y)
etc_preds = etc.predict(test_x)

etc_preds_proba = etc.predict_proba(train_x)
etc_preds_proba_test = etc.predict_proba(test_x)

etc_preds_test = etc.predict_proba(X_test_scaled)
etc_preds_train = etc.predict_proba(X_train_scaled)

print (give_accuracy(test_y, etc_preds))
print (params)
print("Best params & associated score", etc.best_params_, etc.best_score_)


# In[179]:

#gradient boosting classifier
params = {'loss': ['exponential'], 'n_estimators':[50]}
gbc = GridSearchCV(GradientBoostingClassifier(max_depth=7), params, n_jobs = -1, cv=6)
gbc.fit(train_x, train_y)
gbc_preds = gbc.predict(test_x)


gbc_preds_proba = gbc.predict_proba(train_x)
gbc_preds_proba_test = gbc.predict_proba(test_x)

gbc_preds_test = gbc.predict_proba(X_test_scaled)
gbc_preds_train = gbc.predict_proba(X_train_scaled)

print (give_accuracy(test_y, gbc_preds))
print (params)
print("Best params & associated score", gbc.best_params_, gbc.best_score_)


# In[183]:

params = {'activation': ['relu'],
          'alpha': [0.001],
          'solver': ['adam'],
        'hidden_layer_sizes': [(20,7,3)]}
mlp = GridSearchCV(MLPClassifier(), params, n_jobs = -1, cv=8, scoring = 'accuracy')
mlp = mlp.fit(train_x, train_y)
mlp_preds = mlp.predict(test_x)


mlp_preds_proba = mlp.predict_proba(train_x)
mlp_preds_proba_test = mlp.predict_proba(test_x)

mlp_preds_test = mlp.predict_proba(X_test_scaled)
mlp_preds_train = mlp.predict_proba(X_train_scaled)

print(give_accuracy(test_y, mlp_preds))
print(params)
print("Best params & associated score", mlp.best_params_, mlp.best_score_)


# ### Stacking

# In[265]:

new_x_train = pd.DataFrame({
    'rf': rfm_preds_train[:,0],
    'mlp': mlp_preds_train[:,0],
    #'bc': bc_preds_train[:,0],
    'lr': lr_preds_train[:,0],
    'xgb': xgb_preds_train[:,0],
    'gbc': gbc_preds_train[:,0],
    'svc': svc_preds_train[:,0],
    #'etc': etc_preds_train[:,0]
})

new_x_test = pd.DataFrame({
    'rf': rfm_preds_test[:,0],
    'mlp': mlp_preds_test[:,0],
    #'bc': bc_preds_test[:,0],
    'lr': lr_preds_test[:,0],
    'xgb': xgb_preds_test[:,0],
    'gbc': gbc_preds_test[:,0],
    'svc': svc_preds_test[:,0],
    #'etc': etc_preds_test[:,0]
})


# In[258]:

stacked_model = XGBClassifier()
stacked_model = stacked_model.fit(new_x_train, y['Survived'])

stacked_model_preds_train = stacked_model.predict(new_x_train)
stacked_model_preds_test = stacked_model.predict(new_x_test)

#print(give_accuracy(test_y, stacked_model_preds_test))
print(give_accuracy(y['Survived'], stacked_model_preds_train))


# ### RandomForestClassifier performed the best for the stacking models
# ###### So we will use it for making the final predictions
# ### Write to csv

# In[259]:

X_test_passenger_id = pd.read_csv('test_cleaned.csv', usecols=['PassengerId'])
X_test_passenger_id.head()


# In[263]:

my_submission = pd.DataFrame({
    'PassengerId':list(X_test_passenger_id['PassengerId']),
    'Survived': list(stacked_model_preds_test)
})

my_submission.head()


# In[264]:

my_submission.to_csv('submission_18.csv', sep=",", index=False)

