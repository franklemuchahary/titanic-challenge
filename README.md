# README #

### Titanic Challenge on Kaggle

Binary Classification task to get familiar with the basics of Machine Learning. The task is to predict whether a particular 
passenger will survive or not based on other details. Link to the challenge is:

https://www.kaggle.com/c/titanic

I built and ensemble model based on the concept of Stacking and scored an accuracy score of 80.8% on the public leaderboard.
